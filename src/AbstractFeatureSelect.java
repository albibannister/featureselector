import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;

import java.io.*;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by huonf on 9/25/2020.
 */
public abstract class AbstractFeatureSelect {

    double[][] featureSU = null;
    double[] entropies = null;
    double[][] conditionalEnropies = null;
    Instances data = null;
    int m = -1;
    String name;
    int seed;

    Random randomGenerator = null;

    /***
     * Define an enum for search stop conditions
     */
    enum StopCondition {
        EXHAUSTIVE,
        TIME,
        ITERATIONS
    }


    /***
     * Define a method to generate an initial solution
     * Use a default empty solution as we want to try and escape from the local optima of a greedily constructed soln
     * @return
     */
    public boolean[] generateInitialSolution(){
        boolean[] soln = new boolean[m-1];
        for(int i= 0; i < m-1;i++){
            soln[i] = false;
        }
        return soln;

    }

    /***
     * Generate a pertubation by flipping bits
     * @param soln solution to pertubation from
     * @param n  number of bits to flip
     * @return
     */
    public boolean[] generatePerturbation(boolean[] soln, int n){
        boolean[] perturbation = soln.clone();
        for(int i = 0; i <n; i++){
            int randomNum = randomGenerator.nextInt(m-1);
            perturbation[randomNum] = ! perturbation[randomNum];
        }
        return perturbation;
    }

    /***
     * Rule to determine whether we accept a pertubation
     * Use the defauly rule of only accept better.
     * @param oldSoln
     * @param newSoln
     * @return
     */
    public boolean acceptanceCriteria(boolean[] oldSoln, boolean[] newSoln){
        return Objective(newSoln) > Objective(oldSoln);
    }

    /***
     * Run a local search that flips a bit for a certain number of times
     * Tis gives a search that makes the same number of objective function calls
     * @param soln - Initial soln
     * @param n - number of times a bit s flipped
     * @param seed
     * @param record - boolean switch determining whether results of local search should be outputted (used for debugging)
     * @return
     */
    public boolean[] sampleLocalSearch(boolean[] soln, int n, int seed, boolean record) {

        double objective_soln = Objective(soln);
        Random rand = new Random(seed);

        boolean[] best = soln.clone(); //initial solns
        for (int i = 0; i < n; i++) {//do 1000 iteratoans
            boolean[] neighbour = generatePerturbation(best, 1);
            double objective_neighbour = Objective(neighbour);

            if (objective_neighbour > objective_soln) {
                best = neighbour;
                objective_soln = objective_neighbour;
            }
            // System.out.println(objective_soln);
            if (record) {
                //System.out.println(i);
                System.out.println(objective_soln);
            }
        }
        return best;
    }

    /***
     * Run a local search that flips bits until a local optima is reached
     * O(n^2) bits ar flipped
     * @param soln initial solution to start from
     * @param seed
     * @param record - boolean switch determining whether results of local search should be outputted (used for debugging)
     * @return
     */
    public boolean[] exhaustiveLocalSearch(boolean[] soln, int seed,boolean record){

        double objective_soln =  Objective(soln);
        Random rand = new Random(seed);

        boolean[] best = soln.clone();
        while(true){

            //todo randomize this
            List<boolean[]> neighbours = generateNeighbourhood(best);
            for(int j = 0; j <neighbours.size(); j++){
                boolean[] neighbour = neighbours.get(j);
                double objective_neighbour = Objective(neighbour);
                if(objective_neighbour > objective_soln){
                    best  =  neighbour;
                    objective_soln = objective_neighbour;
                    if(record){
                        System.out.println(objective_soln);
                    }
                    break; //first descent
                }else if(j == neighbours.size()-1){
//                    System.out.println(i);//if iterated through all neighbours and found no better soln we have reached optima
                    return best.clone();
                }
                if(record){
                    System.out.println(Objective(best));
                }
            }
        }
    }

    /***
     * initiate a new seed
     * @param seed
     */
    public void setSeed(int seed){
        this.randomGenerator = new Random(seed);
    }

    /**
     *Objective function for all searches; the merit algorithm that divides relevancy by redundancy
     * @param values
     * @return
     */
    public double Objective(boolean[] values){
        double relevence = 0;
        int n = values.length;
        double redundancy = 0;

        Set<Integer> setForm = bitStringToSet(values);
        Integer[] listForm = new Integer[setForm.size()];
        setForm.toArray(listForm);


        for(int i = 0; i < listForm.length; i++) {
                relevence += featureSU[listForm[i]][n];
                for (int j = 0; j < i; j++) {
                        redundancy += featureSU[listForm[i]][listForm[j]];
                }
        }

        return relevence/Math.sqrt(n+2*redundancy);
    }

    /**
     * Calculate SU between any two columns
     * @param x
     * @param y
     * @return
     */
    public double calculateSU(int x, int y){
        double entx = entropies[x];
        double enty = entropies[y];
        double condEnt = conditionalEnropies[x][y];
        if(x==y){
            return 1;
        }
        else if(entx+enty>0){
            return 2*(enty+entx-condEnt)/(entx + enty);
        }
        else{
            return 0;
        }
    }

    /***
     * Calculate the entropy of a discretized list of values
     * @param values
     * @return
     */
    public double calculateEntropy(double[] values){
        Map<Double,Integer> counts = new HashMap<>();
        double n = values.length;
        for(int i = 0; i < values.length; i++){
            double value = values[i];
            if(!counts.containsKey(value)){
                counts.put(value,0);
            }
            counts.put(value,counts.get(value)+1);
        }

        double ent = 0;
        for(Double key : counts.keySet()){
            double prob = counts.get(key)/n;
            ent = ent - prob*Math.log(prob);
        }

        return ent;
    }

    /***
     * Calculate joint entropy between two lists of discretized values
     * @param values1
     * @param values2
     * @return
     */
    public double calculateJointEntropy(double[] values1, double [] values2){
        Map<Double,Map<Double,Integer>> counts = new HashMap<>();
        double n = values1.length;

        for(int i = 0; i < values1.length; i++){
            double value1 = values1[i];
            double value2 = values2[i];

            if(!counts.containsKey(value1)){
                counts.put(value1,new HashMap<>());
            }
            if(!counts.get(value1).containsKey(value2)){
                counts.get(value1).put(value2,0);
            }
            counts.get(value1).put(value2,counts.get(value1).get(value2)+1);
        }

        double ent = 0;
        for(Double key1 : counts.keySet()){
            for(Double key2: counts.get(key1).keySet()){
                double pxy = counts.get(key1).get(key2);
                ent -= pxy/values1.length*Math.log(pxy/values1.length);
            }
        }
        return ent;
    }



    /**
     * Internal method that builds a matrix of SU values from a datasets
     * Should be used first time a dataset is loaded, subsequent calls can just load the SU matrix
     *
     */
    protected void build(){
        //initialise matrices
        this.featureSU = new double[m][m];
        this.entropies = new double[m];
        this.conditionalEnropies = new double[m][m];

            //calcualte entropies and conditional entropies for each feature
            for (int i = 0; i < m; i++) {
                double[] column = data.attributeToDoubleArray(i);
                double ent = calculateEntropy(column);
                entropies[i] = ent;
            }

            System.out.println("Calculating conditional entropies");
            int progress = 0;
            int total = m * m;
            //calculate conditional entropies
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < m; j++) {
                    double[] column1 = data.attributeToDoubleArray(i);
                    double[] column2 = data.attributeToDoubleArray(j);

                    double ent = calculateJointEntropy(column1, column2);
                    conditionalEnropies[i][j] = ent;
                    progress++;
                }
                System.out.println(i);
                if (progress % 10000 == 0) {
                    double percent = (double) progress / (double) total;
                    System.out.println(percent);
                }
            }

            System.out.println("Calculating featureSU");
            //calculate SU for each feature
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < m; j++) {
                    featureSU[i][j] = calculateSU(i, j);
                }
            }
            System.out.println("Built SU Matrix");
    }

    /***
     * Method to convert between a bitstring representation of a solution (for searching) and a set representation
     * (for outputting)
     * @param bitString bitstring form of the soln
     * @return A set of integers.
     */
    protected Set<Integer> bitStringToSet(boolean[] bitString){
       Set set = new TreeSet();
        for(int i =0; i < this.m-1; i++){
            if(bitString[i]){
                set.add(i);
            }
        }
        return set;
    }

    /***
     * Generates a neighbourhood from a single bit flip operations
     *
     * This is really expensive because arrays have to be copied n times (where n is number of features)
     * @param soln
     * @return
     */
    public List<boolean[]> generateNeighbourhood(boolean[] soln){
        List<boolean[]> neighbours = new ArrayList<>();

        for(int i = 0; i < soln.length;i++){
            boolean[] neighbour = soln.clone();
            neighbour[i] = ! neighbour[i];
            neighbours.add(neighbour);
        }
        return neighbours;
    }

    /***
     * Save SU matrix to a file
     * Should only be called first time a data set is ran.
     * @param filename
     */
    public void saveMatrix(String filename){
        BufferedWriter outputWriter = null;
        try{
            outputWriter = new BufferedWriter(new FileWriter(filename));
            int n = featureSU.length;
            int m = featureSU[0].length;
            outputWriter.write(Integer.toString(m));
            outputWriter.write(",");
            outputWriter.write(Integer.toString(n));
            outputWriter.write("\n");

            for(int i = 0; i <featureSU.length; i++){
                double[] row = featureSU[i];
                for(int j =0; j < featureSU[0].length;j++){
                    outputWriter.write(Double.toString(row[j]));
                    if(j < featureSU[0].length-1) {
                        outputWriter.write(",");
                    }
                }
                if(i < featureSU.length-1) {
                    outputWriter.write("\n");
                }
            }
            outputWriter.close();
        }catch (IOException e){
        }
    }


    /***
     * RSort a list of solutions by population
     * @param map
     * @return
     */
    public static LinkedHashMap<boolean[], Double> sortByObjective(LinkedHashMap<boolean[], Double> map) {
        List<Map.Entry<boolean[], Double>> capitalList = new LinkedList<>(map.entrySet());

        capitalList.sort(Map.Entry.comparingByValue());

        LinkedHashMap<boolean[], Double> result = new LinkedHashMap<>();
        for (Map.Entry<boolean[], Double> entry : capitalList)
        {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    /***
     * Return the best solution from a population of solns
     * @param solutions
     * @return
     */
    public boolean[] getBestSolution(LinkedHashMap<boolean[], Double> solutions){
        Iterator<Map.Entry<boolean[], Double>> iterator = solutions.entrySet().iterator();
        Map.Entry<boolean[], Double> best = iterator.next();
        while(iterator.hasNext()){
            Map.Entry<boolean[], Double> currentEntry = iterator.next();
            if(currentEntry.getValue()>best.getValue()){
                best = currentEntry;
            }
        }
        return best.getKey().clone();
    }


    /**
     * Function to run a search. implemented differently for each child class.
     * @param stopCondition -what stopping criteria should we use
     * @param T - parameter for stopping criterion
     * @param threads - parameter for number of threads
     * @param numIterations - number of times to run search
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public abstract Solns search(StopCondition stopCondition, double T,int threads,int numIterations) throws InterruptedException, ExecutionException;
}
