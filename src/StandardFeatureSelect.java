import weka.core.*;

/**
 * Created by huonf on 9/28/2020.
 */
public class StandardFeatureSelect extends AbstractFeatureSelect {
    int seed = 1;


    /***
     * Construtor that takes a file, consturcts an SU matrix and then runs search
     * @param data1
     * @param seed
     * @param fileName
     */
    public StandardFeatureSelect(Instances data1, int seed, String fileName) {
        this.data = data1;
        this.m = this.data.numAttributes();
        setSeed(seed);
        build();
        saveMatrix(fileName);
        this.name = fileName;
        this.seed = seed;


    }

    /**
     * Constuctor that takes a loaded SU marix and runs a search
     * @param features
     * @param seed
     */
    public StandardFeatureSelect(double[][] features, int seed) {
        this.featureSU = features;
        this.m = featureSU.length;
        setSeed(seed);
        //this.name = file;
       // System.out.println("Built SU");
    }


    @Override
    public Solns search(StopCondition stopCondition, double T, int numThreads, int numIterations) {

        //initialse list of solns
        Solns solns = new Solns();

        for (int i = 0; i < numIterations; i++) { //for each iteration
            //construct an iniital solns, initiate timer, stopping condition
            long start = System.currentTimeMillis();
            boolean[] soln = generateInitialSolution();
            double merit = Objective(soln);
            double t;
            if (stopCondition == StopCondition.ITERATIONS) {
                t = 0;
            } else if (stopCondition == StopCondition.EXHAUSTIVE) {
                t = start;
            } else {//exhaustive
                t = 0;
            }
            //loop untiil stopping criteria met
            while (true) {

                boolean[] pertubation = generatePerturbation(soln, this.m/4); //perturate
                //run a search then pertuba
                soln = sampleLocalSearch(soln, 1000, seed, false);

                if (acceptanceCriteria(soln, pertubation)) { //acceptance criterion
                    soln = pertubation;
                }

                //check stopping criteria
                if (stopCondition == StopCondition.ITERATIONS) {
                    t++;
                    if (t >= T) {
                        break;
                    }
                } else if (stopCondition == StopCondition.TIME) {
                    if (System.currentTimeMillis() - t >= T) {
                        if (t >= T) {
                            break;
                        }
                    }
                } else {
                    break; //todo implement exhaustive/convergence
                }
            }
            //add to solns list
            long end = System.currentTimeMillis();
            //output soln and print
            long duration = end - start;
            //System.out.println("Search took " + duration + " ms");
            merit = Objective(soln);
            //System.out.println();
            solns.addSoln(new Soln(merit, bitStringToSet(soln), duration));
        }
        return solns;
    }
}