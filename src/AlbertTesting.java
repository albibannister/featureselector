import java.util.concurrent.ExecutionException;

public class AlbertTesting {

    static final String PATH = "C:\\Users\\albib\\Documents\\largedata\\matrices";
    public static void main(String[] args) throws InterruptedException, ExecutionException {

        //Instances data = IO.loadArff("C:\\Users\\huonf\\workspace\\556Assig\\data\\balance-scale.arff"); //balance scale selects 0,1,2,3
        //Instances data = IO.loadArff("C:\\Users\\huonf\\workspace\\556Assig\\data\\breast-w.arff"); //breast is all true
        //Instances data = IO.loadArff("C:\\Users\\huonf\\workspace\\556Assig\\data\\hayes-roth.arff"); //hayes roth discards 0
//        Instances data = IO.loadArff("C:\\Users\\albib\\IdeaProjects\\556Assig\\data\\sat.arff"); //letter discards 0 and 1
//        Instances data = IO.loadArff("C:\\Users\\albib\\IdeaProjects\\556Assig\\data\\waveform.arff"); //letter discards 0 and 1
//        Instances data = IO.loadArff("C:\\Users\\albib\\Documents\\largedata\\gisette"); //letter discards 0 and 1
        String matrixFile = "C:\\Users\\albib\\Documents\\largedata\\matrices\\madelon";
        String path = "C:\\Users\\albib\\Documents\\largedata";
        String[] fileNames = {"iris","waveform","madelon","gisette","arcene"};
        String[] bigData = {"madelon","gisette","arcene"};
        double[] shortTimes = {0.05,0.1,0.15,0.2,0.25};
        double[] longTimes = {60};

//        AbstractFeatureSelect fs = new ParallelFeatureSelect(matrixFile,0);
//        Solns sols = fs.search(AbstractFeatureSelect.StopCondition.TIME,10,4,1000);
//        IteratedLocalSearch();
//        testSample(fileNames,new double[]{10,20,30,40,50,60},new int[]{4},new int[]{300});
//        testSample(new String[]{"gisette"},new double[]{120},new int[]{4},1);
//        testSample(new String[]{"gisette"},30,new int[]{2,4},30);
//        testSample(new String[]{"iris","waveform"},30,new int[]{2,4},30);
        compareSearches(fileNames,0);




//        testSample(new String[]{"arcene"},new double[]{60},new int[]{4},new int[]{300});
//        testSplit(fileNames,new double[]{},new int[]{1,2,4,6,8},new int[]{300});


    }



    public static void compareSearches(String[] fileNames, int seed)throws ExecutionException, InterruptedException {

        int max_resources = 64;
        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = PATH+"\\"+fileNames[i];
            double[][] file = IO.loadMatrix(data);
            //search
            AbstractFeatureSelect its = new StandardFeatureSelect(file, seed);
            AbstractFeatureSelect pls = new ParallelFeatureSelect(data,seed);

            Solns itssolns = its.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources, 1,30);
            Solns plssolns = pls.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources/4, 4,30);
            //write results to a csv
            IO.saveOutput(itssolns, "compare_time_its" +data + "_" + Integer.toString(max_resources) + "_results.csv");
            IO.saveOutput(plssolns, "compare_time pls" +data + "_" + Integer.toString(max_resources) + "_results.csv");
        }

        max_resources = 35;
        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = fileNames[i];
            double[][] file = IO.loadMatrix(data);
            //search
            AbstractFeatureSelect its = new StandardFeatureSelect(file, seed);
            AbstractFeatureSelect pls = new ParallelFeatureSelect(data,seed);

            Solns itssolns = its.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources, 1,30);
            Solns plssolns = pls.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources, 8,30);
            //write results to a csv
            IO.saveOutput(itssolns, "compare_performance_its" +data + "_" + Integer.toString(max_resources) + "_results.csv");
            IO.saveOutput(plssolns, "compare_performance_pls" +data + "_" + Integer.toString(max_resources) + "_results.csv");
        }
    }

    public static void IteratedLocalSearch() throws ExecutionException, InterruptedException{
        String[] fileNames = {"iris","waveform","madelon","gisette"/*"arcene"*/};
        String path = "C:\\Users\\albib\\Documents\\largedata\\matrices";

        int[] numIts = new int[]{10,30,50,100,500};

        for(int i = 0; i < 4;i++) {

            String data = fileNames[i]; //letter discards 0 and 1
            for(int j = 0; j < numIts.length; j++){
                int numIterators = numIts[j];
                String outputFileName = data+"_"+Integer.toString(numIterators)+"_results.csv";
                AbstractFeatureSelect std = new ParallelFeatureSelect(path + "\\" + data, 3);
                Solns solns = std.search(AbstractFeatureSelect.StopCondition.TIME, 20, 1, numIterators);
                IO.saveOutput(solns,outputFileName);
            }
            ;
        }


    }

    public static void testSample(String[] files, double time, int[] numThreads,int iterations) throws ExecutionException, InterruptedException {
        for (String file : files) {
                for (int threads : numThreads) {
                        AbstractFeatureSelect std = new ParallelFeatureSelect(PATH + "\\" + file,0);
                        String outputFile = file+"_"+"T"+time+"_"+"n"+threads+"_results.csv";
                        Solns solns = std.search(AbstractFeatureSelect.StopCondition.TIME, time, threads, iterations);
                        IO.saveOutput(solns,outputFile);
                }
            }
    }

    public static void testSplit(String[] files, double[] time, int[] numThreads, int[] taskIterations) throws ExecutionException, InterruptedException {
        for (String file : files) {
            AbstractFeatureSelect std = new NHSplitSearch(PATH + "\\" + file);
            for (double T : time) {
                for (int threads : numThreads) {
                    for (int iterations : taskIterations) {
                        String outputFile = file+"_"+"T"+T+"_"+"n"+threads+"_"+"it"+iterations+"_results.csv";
                        Solns solns = std.search(AbstractFeatureSelect.StopCondition.TIME, T, threads, iterations);
                        IO.saveOutput(solns,outputFile);
                    }
                }
            }
        }
    }


    public static void varyThreads(int[] threadCounts) throws ExecutionException, InterruptedException{
        String[] fileNames = {"iris","waveform","madelon","gisette"/*"arcene"*/};
        String path = "C:\\Users\\albib\\Documents\\largedata\\matrices";

        int[] numThreads = new int[]{1,2,4,6};

        for(int i = 0; i < 4;i++) {

            String data = fileNames[i]; //letter discards 0 and 1
            for (int threads : numThreads) {
                String outputFileName = data+"_"+Integer.toString(threads)+"_results.csv";
                AbstractFeatureSelect std = new ParallelFeatureSelect(path + "\\" + data, 3);
                Solns solns = std.search(AbstractFeatureSelect.StopCondition.TIME, 30, threads, 30);

                        ;
                IO.saveOutput(solns,outputFileName);
            }
            ;
        }


    }
}
