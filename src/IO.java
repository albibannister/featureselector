/**
 * Created by huonf on 9/25/2020.
 */

import weka.classifiers.evaluation.output.prediction.Null;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;
import java.io.IOException;
import java.io.*;
import java.util.List;


public class IO {

    /***
     * Load Arff file
     * @param fileName
     * @return
     */
    public static Instances loadArff(String fileName){
        try {
            System.out.println("Attempting to load "+fileName);
            ArffLoader loader = new ArffLoader();
            loader.setFile(new File(fileName));
            Instances data = loader.getDataSet();
            System.out.println("Finished loading data");
            return data;
        }
        catch (IOException e){
            return null;
        }
    }

    /***
     * Save a list of solns to a file
     * @param solns
     * @param fileName
     */
    public static void saveOutput(Solns solns, String fileName){
        try{

            System.out.println(solns.bestSoln());
            File outputFile = new File(fileName);
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write("Merit,Run_Time");

            List<Soln> solnsList = solns.getSolns();
            for(int j = 0; j < solnsList.size();j++){
                Soln soln = solnsList.get(j);
                writer.write("\n");
                double merit = soln.merit;
                double time = soln.timeStamp;
                writer.write(Double.toString(merit));
                writer.write(",");
                writer.write(Double.toString(time));
            }
            writer.close();
        }catch(IOException e){

        }
    }

    /***
     * Load an SU matrix with a given filename
     * @param filename
     * @return
     */
    public static double[][] loadMatrix(String filename){
        BufferedReader outputReader = null;
        try{
            outputReader = new BufferedReader(new FileReader(filename));
            String header = outputReader.readLine();
            String[] headerSplit = header.split(",");
            int m = Integer.parseInt(headerSplit[0]);
            int n = Integer.parseInt(headerSplit[1]);
            double[][] features = new double[n][m];

            for(int i = 0; i < n; i++){
                String line = outputReader.readLine();
                String[] lineSplit = line.split(",");
                for(int j = 0; j < m; j++) {
                    double d = Double.parseDouble(lineSplit[j]);
                    features[i][j] = d;
                }
            }
            System.out.println("Built SU");
            return features;
        }catch (IOException e){
            System.out.println("Failed to build SU");
            return null;
        }
    }

}
