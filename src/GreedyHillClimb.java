import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by huonf on 10/24/2020.
 */
public class GreedyHillClimb extends AbstractFeatureSelect {

    public GreedyHillClimb(double[][] features){
        this.featureSU = features;
        this.m = featureSU.length;
    }

    /***
     * Geet the SU value for any particular feature, used for sorting features by relevancy
     * @param i
     * @return
     */
    public double getSU(int i){
        return featureSU[i][m-1];
    }

    @Override
    public Solns search(StopCondition stopCondition, double T,int threads,int numIterations){
        long start = System.currentTimeMillis();
        boolean[] soln = generateInitialSolution();
        double merit = Objective(soln);

        //sort feature by SU
        List<Integer> features = new ArrayList<>();
        for(int i = 0; i < m-1;i++){
            features.add(i);
        }
        Collections.sort(features, new sortBySU());

        //try adding each feature, (greedy O(n) search)
        for(int i = 0; i < m-1;i++){
            Integer next_feature = features.get(i);
            soln[next_feature] = true;
            double new_merit = Objective(soln);
            if(new_merit > merit){
                merit = new_merit;
            }else{
                soln[next_feature] = false;
            }
        }

        //record and output soln
        long end = System.currentTimeMillis();
        Set<Integer> setForm  = bitStringToSet(soln);
        Soln finalSoln = new Soln(merit,setForm,end-start);
        System.out.println(finalSoln);
        Solns slns = new Solns();
        slns.addSoln(finalSoln);
        return slns;

    }

    /***
     * Comparator to sort a a list of features by relevancy
     */
    class sortBySU implements Comparator<Integer>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(Integer a, Integer b)
        {
            double diff = getSU(a) - getSU(b);
            if(diff == 0){
                return 0;
            }else if(diff <0){
                return 1;
            }else {
                return 0;
            }
        }
    }
    //add featues if merit increases



}
