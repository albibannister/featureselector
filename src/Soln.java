import java.util.Set;

/**
 * Created by huonf on 10/20/2020.
 */
public class Soln {
    public double merit;
    public Set soln;
    public double timeStamp;

    public Soln(double merit, Set soln,double runTime){
        this.merit = merit;
        this.soln = soln;
        this.timeStamp = runTime;

    }

    public String toString(){
        return "Solution is " + soln.toString() + ", with merit " + Double.toString(merit);
    }
}
