import weka.core.Instances;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

public class NHSplitSearch extends AbstractFeatureSelect {

    public NHSplitSearch(Instances data, String matrixFile) {
        this.data = data;
        this.featureSU = IO.loadMatrix(matrixFile);
        setSeed(0);
    }
    public NHSplitSearch(String matrixFile) {
        this.featureSU = IO.loadMatrix(matrixFile);
        setSeed(0);
    }

    @Override
    public Solns search(StopCondition stopCondition,double T, int numThreads,int numIterations) throws InterruptedException, ExecutionException {
        long startTime = System.nanoTime();
        ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
        Solns solns = new Solns();
        boolean[] best = generateInitialSolution();
//        repeat until converged
        int n = m-1;
        int chunkSize = n/numThreads;
        int rem = n%chunkSize;
        List<NHSplitSearchTask> tasks = new ArrayList<>();

        for (int i = 0; i < n; i+=chunkSize) {
//            for last iteration make sure remainder is included
            if(i+chunkSize > n){
                tasks.add(new NHSplitSearchTask(i,i+rem,best));
            }
            else{
                tasks.add(new NHSplitSearchTask(i,i+chunkSize,best));
            }
        }

        double currentTime = 0;
        int counter = 0;
        while(true){
//            System.out.println("iteration");
            List<boolean[]> neighbourhood = generateNeighbourhood(best);
            Collections.shuffle(neighbourhood,this.randomGenerator);
            List<Future<boolean[]>> results = new ArrayList<>();

            for (NHSplitSearchTask task : tasks) {
                task.setNeighbourhood(neighbourhood);
                task.setPrev(best);
                results.add(threadPool.submit(task));
            }

            boolean finished = false;

//            check tasks for one which has finished
            while(!finished){
                for (Future<boolean[]> result : results) {
                    if(result.isDone()){
                        boolean[] sol = result.get();
                        System.out.println(Objective(sol));
                        best = sol.clone();
                        finished = true;
                        break;
                    }
                }
            }

            for (Future<boolean[]> result : results) {
                result.cancel(true);
            }
            currentTime = (System.nanoTime() - startTime)/1e9;
            solns.addSoln(new Soln(Objective(best), bitStringToSet(best),currentTime));

            if(stopCondition == StopCondition.TIME){
                if(currentTime > T){
                    break;
                }
            }
            if(stopCondition == StopCondition.ITERATIONS){
                counter++;
                if(counter>T){
                    break;
                }
            }
        }
        threadPool.shutdown();
        return solns;
    }

    class NHSplitSearchTask implements Callable<boolean[]> {
        int startIndex;
        int endIndex;
        boolean improvement;

        List<boolean[]> neighbourhood;
        boolean[] prev;

        public NHSplitSearchTask(int startIndex, int endIndex,boolean[] prev) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.prev = prev;
        }

        @Override
        public boolean[] call() throws Exception {

            for (int i = startIndex; i < endIndex; i++){
                boolean[] neighbour = this.neighbourhood.get(i);

                if(acceptanceCriteria(prev,neighbour)){
//                    System.out.println("found improvement");
//                    System.out.println(Objective(neighbour));
                    improvement = true;
                    return neighbour;
                }
            }

            return prev;
        }

        public void setNeighbourhood(List<boolean[]> neighbourhood) {
            this.neighbourhood = neighbourhood;
        }

        public void setPrev(boolean[] prev) {
            this.prev = prev;
        }
    }
}
