import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by huonf on 10/21/2020.
 */
public class Solns {
    List<Soln> solns = null;
    List<Double> runTimes = null;
    List<Double> merits = null;

    public Solns(){
        solns = new ArrayList();
        runTimes = new ArrayList<>();
        merits = new ArrayList<>();

    }

    public List<Soln> getSolns(){
        return solns;
    }

    public void addSoln(Soln soln){
        solns.add(soln);
        runTimes.add(soln.timeStamp);
        merits.add(soln.merit);
    }

    public double meanRuntime(){
        double sum = 0;
        for(int i = 0; i < solns.size();i++){
            sum+= runTimes.get(i);
        }
        return sum/solns.size();
    }

    public double sdRuntime(){
        double mean = meanRuntime();
        double ss = 0;
        for(int i = 0; i < solns.size();i++){
            ss += (runTimes.get(i)-mean)*(runTimes.get(i)-mean);
        }
        return Math.sqrt(ss/(solns.size()-1));
    }

    public double meanMerit(){
        double sum = 0;
        for(int i = 0; i < solns.size();i++){
            sum+= merits.get(i);
        }
        return sum/solns.size();
    }

    public double sdMerit(){
        double mean = meanMerit();
        double ss = 0;
        for(int i = 0; i < solns.size();i++){
            ss += (merits.get(i)-mean)*(merits.get(i)-mean);
        }
        return Math.sqrt(ss/(solns.size()-1));
    }

    public Soln bestSoln(){
        Soln best = null;
        double bestMerit = -1;
        for(int i = 0; i < solns.size(); i++){
            if(solns.get(i).merit>bestMerit){
                best = solns.get(i);
                bestMerit = solns.get(i).merit;
            }
        }
        return best;
    }

}