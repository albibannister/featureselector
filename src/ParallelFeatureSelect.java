import weka.core.Instances;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by huonf on 9/28/2020.
 */
public class ParallelFeatureSelect extends AbstractFeatureSelect {

    public ParallelFeatureSelect(Instances data1, int seed){
        this.data = data1;
        this.m =  this.data.numAttributes();
        this.seed = seed;

        setSeed(seed);
        build();
    }

    public ParallelFeatureSelect(String file, int seed) {
        this.featureSU = IO.loadMatrix(file);
        this.m = featureSU.length;
        setSeed(seed);
    }

    public Solns search(StopCondition stopCondition,double T, int numThreads,int numIterations) throws InterruptedException, ExecutionException {
//        System.out.println("Beginning search");
//        System.out.println("time limit: "+T);
//        System.out.println("Threads "+numThreads);
        int n = this.m-1; //n is number of features to search
        Solns solns = new Solns();

        for (int j = 0; j < numIterations; j++) {
            setSeed(j);
            long startTime = System.nanoTime();
            ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
//        initialise
            boolean[] currentBest = generateInitialSolution();
            IteratedSearchTask[] tasks = new IteratedSearchTask[numThreads];

            for (int i = 0; i < tasks.length; i++) {
                tasks[i] = new IteratedSearchTask(generateInitialSolution(), this.seed + i, 1);
            }

            int counter = 0;
            int iterationsWithoutImprovement = 0;
            while (true) {

                if (stopCondition == StopCondition.TIME) {
                    double currentTime = (System.nanoTime() - startTime) / 1e9;
                    if (currentTime > T) {
                        break;
                    }
                }

                ArrayList<Future<boolean[]>> results = new ArrayList<>();

                for (IteratedSearchTask task : tasks) {
                    results.add(threadPool.submit(task));
                }

                boolean finished = false;
                //                check through running tasks until all are finished
                while (!finished) {
                    finished = true;
                    for (Future<boolean[]> result : results) {
                        if (!result.isDone()) {
                            finished = false;
                        }
                    }
                }

                LinkedHashMap<boolean[], Double> solutions = new LinkedHashMap<>();
//            store solution and objective function value, then sort map in order of objective fn

                for (Future<boolean[]> result : results) {
                    boolean[] sol = result.get().clone();
                    double obj = Objective(sol);
//                System.out.println(obj);
//                System.out.println(Arrays.toString(sol));
                    solutions.put(sol, obj);
                }

//                solutions = sortByObjective(solutions);

//            sort tasks by current solution quality
//                Arrays.sort(tasks);

                boolean[] iterationBest = getBestSolution(solutions);
                boolean improvement = false;
                if (acceptanceCriteria(currentBest, iterationBest)) {
                    improvement = true;
                    iterationsWithoutImprovement = 0;
                    currentBest = iterationBest;
//                    System.out.println("Best solution from iteration: " + Objective(currentBest));

//                restart tasks from new best solution
                    for (int i = 0; i < tasks.length; i++) {
                        tasks[i].updateSolution(currentBest.clone());
                    }
                }

                if (!improvement) {
                    iterationsWithoutImprovement++;
                    if (iterationsWithoutImprovement > 100) {
                        break;
                    }
                }

                if (stopCondition == StopCondition.ITERATIONS) {
                    counter += 1;
                    if (counter > T) {
                        break;
                    }
                }
            }

            threadPool.shutdown();

            //output soln and print
//            System.out.println(Arrays.toString(currentBest));
//            System.out.println(Objective(currentBest));

            double duration = (System.nanoTime() - startTime) / 1e9;

//            System.out.println("Search took " + duration + " ms");
            solns.addSoln(new Soln(Objective(currentBest), bitStringToSet(currentBest), duration));
        }
        return solns;
    }

//    Task class for submitting to threadpool
    class IteratedSearchTask implements Callable<boolean[]>, Comparable<IteratedSearchTask> {
        boolean[] solution;
        int seed;
        int iterations;
        double objective;
        int perturbationFactor;

//        task which perturbates a solution then runs local search
        public IteratedSearchTask(boolean[] initialSolution, int seed, int iterations){
            this.solution = initialSolution;
            this.objective = Objective(initialSolution);
            this.seed = seed;
            this.iterations = iterations;
            this.perturbationFactor = solution.length/100+1   ;
        }

        public boolean[] getSolution() {
            return solution;
        }


        public void updateSolution(boolean[] newSolution){
            this.objective = Objective(newSolution);
            this.solution = newSolution;
        }

        @Override
        public boolean[] call() throws Exception {
            for (int i = 0; i < iterations; i++) {
                boolean[] perturbation = generatePerturbation(solution,perturbationFactor);
                boolean[] searched = sampleLocalSearch(perturbation,1000,this.seed,false);
                if(acceptanceCriteria(solution,searched)){
                    updateSolution(searched);
                }
            }
            return solution;
        }

        @Override
        public int compareTo(IteratedSearchTask other) {
            return Double.compare(this.objective,other.objective);
        }
    }

}
