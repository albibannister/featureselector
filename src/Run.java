import java.util.concurrent.ExecutionException;

/**
 * Created by huonf on 9/25/2020.
 */


public class Run {

    /***
     * Function to compare a iterated local search with its parallel version by a fixed time
     * @param fileNames
     * @param seed
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void compareByTime(String[] fileNames, int seed)throws ExecutionException, InterruptedException {
        int max_resources = 35;
        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = fileNames[i];
            double[][] file = IO.loadMatrix(data);
            //search
            AbstractFeatureSelect its = new StandardFeatureSelect(file, seed);
            AbstractFeatureSelect pls = new ParallelFeatureSelect(data,seed);

            Solns itssolns = its.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources, 1,30);
            Solns plssolns = pls.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources*1000, 8,30);
            //write results to a csv
            IO.saveOutput(itssolns, "compare_performance_its" +data + "_" + Integer.toString(max_resources) + "_results.csv");
            IO.saveOutput(plssolns, "compare_performance  pls" +data + "_" + Integer.toString(max_resources) + "_results.csv");
        }
    }

    /***
     * Function to compare a iterated local search with its parallel version by a fixed number of restarts
     * @param fileNames
     * @param seed
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void compareByIts(String[] fileNames, int seed)throws ExecutionException, InterruptedException {

        int max_resources = 64;
        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = fileNames[i];
            double[][] file = IO.loadMatrix(data);
            //search
            AbstractFeatureSelect its = new StandardFeatureSelect(file, seed);
            AbstractFeatureSelect pls = new ParallelFeatureSelect(data,seed);

            Solns itssolns = its.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources, 1,30);
            Solns plssolns = pls.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  max_resources/8, 8,30);
            //write results to a csv
            IO.saveOutput(itssolns, "compare_time_its" +data + "_" + Integer.toString(max_resources) + "_results.csv");
            IO.saveOutput(plssolns, "compare_time pls" +data + "_" + Integer.toString(max_resources) + "_results.csv");
        }
    }





    /***
     * Method to run an iterated local search
     * Loopse each file and desired number of iterations
     * Change things in this method to change search
     *
     * @throws ExecutionException error needed for parallistion
     * @throws InterruptedException error needed for parallalisation
     */
    public static void IteratedLocalSearch(String[] fileNames, int seed) throws ExecutionException, InterruptedException {
        int[] numIts = new int[]{100};

        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = fileNames[i];
            double[][] file = IO.loadMatrix(data);
            for (int j = 0; j < numIts.length; j++) { //iterate over number of iterations
                //search
                int numIterators = numIts[j];
                AbstractFeatureSelect std = new StandardFeatureSelect(file, seed);
                Solns solns = std.search(AbstractFeatureSelect.StopCondition.ITERATIONS,  numIterators, 1,30);
                //write results to a csv
                IO.saveOutput(solns, data + "_" + Integer.toString(numIterators) + "_results.csv");
            }
        }


    }

    /***
     * Function to run the greedy hill climbing approach
     * @param fileNames
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void HillClimbSearch(String[] fileNames) throws ExecutionException, InterruptedException {


        for (int i = 0; i < fileNames.length; i++) {//iterate over files
            String data = fileNames[i];
            double[][] file = IO.loadMatrix(data);//iterate over number of iterations
                //search
                AbstractFeatureSelect std = new GreedyHillClimb(file);
                Solns solns = std.search(AbstractFeatureSelect.StopCondition.ITERATIONS, 100, 1, 30);
                //write results to a csv
                IO.saveOutput(solns, "greedy_hill_climb"+data + "_" + "_results.csv");
            }
    }

    public static void testParallelSample(String[] files, double time, int[] numThreads,int iterations) throws ExecutionException, InterruptedException {
        for (String file : files) {
            for (int threads : numThreads) {
                AbstractFeatureSelect std = new ParallelFeatureSelect(file,0);
                String outputFile = file+"_"+"T"+time+"_"+"n"+threads+"_results.csv";
                Solns solns = std.search(AbstractFeatureSelect.StopCondition.TIME, time, threads, iterations);
                IO.saveOutput(solns,outputFile);
            }
        }
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String[] fileNames = {"iris","waveform","madelon","gisette","arcene"};
        //HillClimbSearch(fileNames);
        IteratedLocalSearch(fileNames,1);
    }

}